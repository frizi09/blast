goals:
- inspector should allow modifying owned values
- inspector should inspect owned data recursively
- user must be able to provide inspectors for types in external crates (think vector library)
- creating inspector must be easy
- inspector can access external state to save/load things (moderately easy with ctx, separate inspector world?)


maybe:
- missing inspectors would show up with some text
    - maybe use "Debug" if available? Is this even possible without specialization>?
    - possibly use intrinsic type_name on nightly for better debug


maybe for non-owned:
- non-owned data could be displayed read-only

ideas:
- derive(Inspect) would just create a dedicated inspector struct with Inspector trait implementation, not derive a trait on component itself.
    - that way it's still possible to create inspectors for types from other crates, because we only work with local struct
    - possibly a macro! can be created for external types


// OLD NOTES
trait for `Reflect` with derive
- list all fields and gives the name + accessor back
    - how to handle data types that are not Reflect

Inspector<T: Reflect> would be the default generated one

Inspector::register()
DefaultInspector {
    next: Option<Inspector>
}



enum OwnerComponentField {
    F0
}

static VELOCITY_FIELDS: &[VelocityComponentField] = [VelocityComponentField::X, VelocityComponentField::Y];
enum VelocityComponentField {
    X
    Y
}

trait InspectField<T> {
    fn every () -> &[Self] {
        VELOCITY_FIELDS
    }
}


trait Inspector {
    type Field;
    fn inspect_field(&mut self, f: Field);
    fn inspect(&mut self) {

        for f in Self::Field::every().iter() {
            self.inspect_field()
        }
    }
    fn stack(prev: Inspector) -> Inspector;


impl Inspector for {
    fn inspect_field(f: Field) {

    }

    // default
    fn inspect(fs: &mut [Fields]) {
        for f in fs.iter_mut() {
        }
    }
}

use inspector stack, so each inspector can override existing one
inspector per type?

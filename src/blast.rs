use amethyst::assets::{AssetStorage, Loader};
use amethyst::core::cgmath::Vector3;
use amethyst::core::transform::TransformBundle;
use amethyst::core::{GlobalTransform, Parent, Transform};
use amethyst::prelude::*;
use amethyst::renderer::{
    Blend, BlendValue, ColorMask, DepthMode, DrawFlat, Equation, Event, Factor, Material,
    MaterialDefaults, MaterialTextureSet, Mesh, MeshHandle, PosTex, RenderBundle, SpriteSheet,
    ALPHA,
};
use amethyst::{GameDataBuilder, Result, StateData};
use blueprint::{Blueprints, ProjectileBlueprint};
use features::abilities::*;
use features::components::*;
use features::{BlastBundle, SpellPos};
use game_config::GameConfig;
use input::BlastInputBundle;
use inspector::InspectorBundle;
use player_anim::{self, PlayerAnimBundle, PlayerAnimControlSet};
use png_loader;
use sprite;
use sprite_sheet_loader;
use std::collections::HashMap;

const ARENA_WIDTH: f32 = 1280.0;
const ARENA_HEIGHT: f32 = 720.0;
const PLAYER_COLOR: [f32; 4] = [0.8, 0.2, 0.2, 1.0];

pub struct Blast;

impl Blast {
    pub fn new() -> Self {
        Blast
    }
}

pub struct BlastGameData<'a, 'b> {
    fixed: GameData<'a, 'b>,
    frame: GameData<'a, 'b>,
}

impl<'a, 'b> BlastGameData<'a, 'b> {
    fn new(fixed: GameData<'a, 'b>, frame: GameData<'a, 'b>) -> Self {
        Self { fixed, frame }
    }
}

pub struct BlastGameDataBuilder<'a, 'b> {
    fixed: GameDataBuilder<'a, 'b>,
    frame: GameDataBuilder<'a, 'b>,
}

impl<'a, 'b> BlastGameDataBuilder<'a, 'b> {
    fn new(fixed: GameDataBuilder<'a, 'b>, frame: GameDataBuilder<'a, 'b>) -> Self {
        Self { fixed, frame }
    }
}

impl<'a, 'b> DataInit<BlastGameData<'a, 'b>> for BlastGameDataBuilder<'a, 'b> {
    fn build(self, world: &mut World) -> BlastGameData<'a, 'b> {
        BlastGameData::new(self.fixed.build(world), self.frame.build(world))
    }
}

pub fn setup_game_data<'a, 'b>(config: GameConfig) -> Result<BlastGameDataBuilder<'a, 'b>> {
    use amethyst::renderer::{Pipeline, Stage};
    use amethyst::ui::{DrawUi, UiBundle};
    use amethyst_imgui::{ImguiBundle, ImguiPass};
    // use gamma_pass::GammaPass;

    let pipe = Pipeline::build()
        // .with_target(Target::named("pre_fx").with_num_color_bufs(1))
        .with_stage(
            // Stage::with_target("pre_fx")
            Stage::with_backbuffer()
                .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
                .with_pass(DrawFlat::<PosTex>::new().with_transparency(
                    ColorMask::all(),
                    ALPHA,
                    Some(DepthMode::LessEqualWrite),
                ))
                .with_pass(DrawUi::new())
                .with_pass(ImguiPass::new()),
        );
    // .with_stage(Stage::with_backbuffer().with_pass(GammaPass::new()));

    let render_bundle = RenderBundle::new(pipe, Some(config.display));

    let path = format!("{}/gamecontrollerdb.txt", env!("CARGO_MANIFEST_DIR"));

    let fixed_game_data = GameDataBuilder::default()
        .with_bundle(
            BlastInputBundle::new()
                .with_bindings(config.bindings)
                .with_sdl_controller_mappings_from_file(path),
        )?
        .with_bundle(PlayerAnimBundle::new(
            "animation_control",
            "sampler_interpolation",
        ))?
        .with_bundle(
            TransformBundle::new().with_dep(&["animation_control", "sampler_interpolation"]),
        )?
        .with_bundle(UiBundle::<String, String>::new())?
        .with_bundle(BlastBundle)?;

    let frame_game_data = GameDataBuilder::default()
        .with_bundle(render_bundle)?
        .with_bundle(ImguiBundle)?
        .with_bundle(InspectorBundle)?;

    Ok(BlastGameDataBuilder::new(fixed_game_data, frame_game_data))
}

impl<'a, 'b> State<BlastGameData<'a, 'b>> for Blast {
    fn handle_event(
        &mut self,
        _state_data: StateData<BlastGameData>,
        event: Event,
    ) -> Trans<BlastGameData<'a, 'b>> {
        use amethyst::renderer::{ElementState, KeyboardInput, VirtualKeyCode, WindowEvent};

        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(key),
                            ..
                        },
                    ..
                } => match key {
                    VirtualKeyCode::Escape => Trans::Quit,
                    _ => Trans::None,
                },
                _ => Trans::None,
            },
            _ => Trans::None,
        }
    }

    fn update(&mut self, state_data: StateData<BlastGameData>) -> Trans<BlastGameData<'a, 'b>> {
        state_data.data.frame.update(&state_data.world);
        Trans::None
    }

    fn fixed_update(
        &mut self,
        state_data: StateData<BlastGameData>,
    ) -> Trans<BlastGameData<'a, 'b>> {
        state_data.data.fixed.update(&state_data.world);
        Trans::None
    }

    fn on_start(&mut self, data: StateData<BlastGameData>) {
        let StateData { world, .. } = data;

        initialise_camera(world);
        initialise_player(world);

        let projectile = ProjectileBlueprint {
            mesh: create_mesh(world, generate_circle_vertices(3.0, 4)),
            material: create_colour_material(world, PLAYER_COLOR),
        };

        world.add_resource(Blueprints { projectile });
    }
}

fn initialise_camera(world: &mut World) {
    use amethyst::core::cgmath::{Matrix4, Vector3};
    use amethyst::core::GlobalTransform;
    use amethyst::renderer::{Camera, Projection};
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            ARENA_WIDTH,
            ARENA_HEIGHT,
            0.0,
        )))
        .with(GlobalTransform(
            Matrix4::from_translation(Vector3::new(0.0, 0.0, 1.0)).into(),
        ))
        .build();
}

pub type SpritesheetSet = HashMap<u64, SpriteSheet>;

fn initialise_player(world: &mut World) {
    // let mesh = create_mesh(world, generate_circle_vertices(PLAYER_RADIUS, 4));
    // let material = create_colour_material(world, PLAYER_COLOR);
    let sprite_sheet_texture = png_loader::load("assets/character_1.png", world);
    let sprite_sheet_definition = sprite::SpriteSheetDefinition::new(64.0, 64.0, 21, 13, false);

    let sprite_sheet_material = {
        let mat_defaults = world.read_resource::<MaterialDefaults>();
        Material {
            albedo: sprite_sheet_texture.clone(),
            ..mat_defaults.0.clone()
        }
    };

    let sprite_mesh = {
        let loader = world.read_resource::<Loader>();
        loader.load_from_data(
            create_mesh_vertices(64.0, 64.0).into(),
            (),
            &world.read_resource::<AssetStorage<Mesh>>(),
        )
    };

    let sprite_sheet_index = 0;
    let sprite_sheet = sprite_sheet_loader::load(sprite_sheet_index, &sprite_sheet_definition);

    world.add_resource(SpritesheetSet::new());

    world
        .write_resource::<SpritesheetSet>()
        .insert(sprite_sheet_index, sprite_sheet);

    world
        .write_resource::<MaterialTextureSet>()
        .insert(sprite_sheet_index, sprite_sheet_texture);

    let tiles_texture = png_loader::load("assets/tiles.png", world);
    let tiles_mat = {
        let mat_defaults = world.read_resource::<MaterialDefaults>();
        Material {
            albedo: tiles_texture.clone(),
            ..mat_defaults.0.clone()
        }
    };

    world
        .create_entity()
        .with(Transform::default())
        .with(GlobalTransform::default())
        .with(tiles_mat)
        .with(Tilemap {
            num_cols: 19,
            #[cfg_attr(rustfmt, rustfmt_skip)]
            tiles: vec![
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  2,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  3,  5,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  3,  6,  5,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  3,  6,  6,  5,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  1,  6,  6,  6,  0,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  6,  6,  6,  6,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  6,  6,  6,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  6,  6,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  6,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
                7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,
            ],
        })
        .build();

    player_anim::prepare_player_animation(world);

    let mut local_transform = Transform::default();
    local_transform.translation = Vector3::new(ARENA_WIDTH / 2.0, ARENA_HEIGHT / 2.0, 0.0);

    let mut caster = SpellCaster::new();
    caster.insert(SpellPos::Trig1, AbilPlasmaBall);

    let player_0 = world
        .create_entity()
        .with(sprite_sheet_material.clone())
        .with(sprite_mesh.clone())
        .with(local_transform.clone())
        .with(GlobalTransform::new())
        .with(PlayerController)
        .with(caster.clone())
        .with(MovementInertia::new())
        .with(Owner(0))
        .with(PlayerAnimControlSet::default())
        .build();

    world
        .create_entity()
        .with(sprite_sheet_material)
        .with(sprite_mesh)
        .with(local_transform)
        .with(GlobalTransform::default())
        .with(PlayerController)
        .with(caster)
        .with(MovementInertia::new())
        .with(Owner(1))
        .with(PlayerAnimControlSet::default())
        .build();

    let orbit_mat = create_colour_material(world, PLAYER_COLOR);
    let orbit_mesh = create_mesh(world, generate_circle_vertices(3.0, 4));
    world
        .create_entity()
        .with(orbit_mat)
        .with(orbit_mesh)
        .with(Transform::default())
        .with(GlobalTransform::default())
        .with(Owner(0))
        .with(Orbit::new(20.0, 0.0, 2.0))
        .with(Parent { entity: player_0 })
        .build();
}

/// Converts a vector of vertices into a mesh.
fn create_mesh(world: &World, vertices: Vec<PosTex>) -> MeshHandle {
    let loader = world.read_resource::<Loader>();
    loader.load_from_data(vertices.into(), (), &world.read_resource())
}

/// Generates vertices for a circle. The circle will be made of `resolution`
/// triangles.
fn generate_circle_vertices(radius: f32, resolution: usize) -> Vec<PosTex> {
    use std::f32::consts::PI;

    let mut vertices = Vec::with_capacity(resolution * 3);
    let angle_offset = 2.0 * PI / resolution as f32;

    // Helper function to generate the vertex at the specified angle.
    let generate_vertex = |angle: f32| {
        let x = angle.cos();
        let y = angle.sin();
        PosTex {
            position: [x * radius, y * radius, 0.0],
            tex_coord: [x, y],
        }
    };

    for index in 0..resolution {
        vertices.push(PosTex {
            position: [0.0, 0.0, 0.0],
            tex_coord: [0.0, 0.0],
        });

        vertices.push(generate_vertex(angle_offset * index as f32));
        vertices.push(generate_vertex(angle_offset * (index + 1) as f32));
    }

    vertices
}

/// Creates a solid material of the specified colour.
fn create_colour_material(world: &World, colour: [f32; 4]) -> Material {
    use amethyst::renderer::MaterialDefaults;

    let mat_defaults = world.read_resource::<MaterialDefaults>();
    let loader = world.read_resource::<Loader>();

    let albedo = loader.load_from_data(colour.into(), (), &world.read_resource());

    Material {
        albedo,
        ..mat_defaults.0.clone()
    }
}

/// Returns a set of vertices that make up a rectangular mesh of the given size.
///
/// This function expects pixel coordinates -- starting from the top left of the image. X increases
/// to the right, Y increases downwards.
///
/// # Parameters
///
/// * `sprite_w`: Width of each sprite, excluding the border pixel if any.
/// * `sprite_h`: Height of each sprite, excluding the border pixel if any.
fn create_mesh_vertices(sprite_w: f32, sprite_h: f32) -> Vec<PosTex> {
    let tex_coord_left = 0.;
    let tex_coord_right = 1.;
    // Inverse the pixel coordinates when transforming them into texture coordinates, because the
    // render passes' Y axis is 0 from the bottom of the image, and increases to 1.0 at the top of
    // the image.
    let tex_coord_top = 0.;
    let tex_coord_bottom = 1.;

    let half_w = sprite_w / 2.0;
    let half_h = sprite_h / 2.0;

    vec![
        PosTex {
            position: [-half_w, -half_h, 0.],
            tex_coord: [tex_coord_left, tex_coord_top],
        },
        PosTex {
            position: [half_w, -half_h, 0.],
            tex_coord: [tex_coord_right, tex_coord_top],
        },
        PosTex {
            position: [-half_w, half_h, 0.],
            tex_coord: [tex_coord_left, tex_coord_bottom],
        },
        PosTex {
            position: [half_w, half_h, 0.],
            tex_coord: [tex_coord_right, tex_coord_bottom],
        },
        PosTex {
            position: [-half_w, half_h, 0.],
            tex_coord: [tex_coord_left, tex_coord_bottom],
        },
        PosTex {
            position: [half_w, -half_h, 0.],
            tex_coord: [tex_coord_right, tex_coord_top],
        },
    ]
}

use amethyst::core::cgmath::Vector2;
use amethyst::core::timing::Time;
use amethyst::core::Transform;

use super::*;

#[derive(Default)]
pub struct PhysicsFeature;

impl<'a, 'b> SystemBundle<'a, 'b> for PhysicsFeature {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(PhysicsDragSystem, "physics_drag", &[]);
        builder.add(PhysicsForceSystem, "physics_force", &["physics_drag"]);
        builder.add(
            PhysicsForceResetSystem,
            "physics_force_reset",
            &["physics_force"],
        );
        builder.add(
            PhysicsVelocitySystem,
            "physics_velocity",
            &["physics_force"],
        );
        Ok(())
    }
}

// COMPONENTS
// Transform: m (meters)

#[derive(Component, Debug, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Velocity(pub Vector2<f32>);
// Velocity: m/s (meters per second)

#[derive(Component, Debug, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Mass(pub f32);
// Mass: kg (kilogram)

#[derive(Component, Debug, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Force(pub Vector2<f32>);
// Force: N = (kg*m)/s^2 (newton)

impl Force {
    pub fn new() -> Self {
        Force(Vector2::new(0.0, 0.0))
    }
}

#[derive(Component, Debug, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Drag(pub f32);
// drag coeff: force =+ drag * -velocity

// SYSTEMS

struct PhysicsVelocitySystem;
struct PhysicsForceSystem;
struct PhysicsForceResetSystem;
struct PhysicsDragSystem;

impl<'s> System<'s> for PhysicsVelocitySystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Velocity>,
        Read<'s, Time>,
    );
    fn run(&mut self, data: Self::SystemData) {
        let (mut transforms, velocities, time) = data;
        let dt = time.delta_seconds();

        for (mut transform, velocity) in (&mut transforms, &velocities).join() {
            transform.translation[0] += velocity.0.x * dt;
            transform.translation[1] += velocity.0.y * dt;
        }
    }
}

impl<'s> System<'s> for PhysicsForceSystem {
    type SystemData = (
        WriteStorage<'s, Velocity>,
        ReadStorage<'s, Force>,
        ReadStorage<'s, Mass>,
        Read<'s, Time>,
    );
    fn run(&mut self, data: Self::SystemData) {
        let (mut velocities, forces, masses, time) = data;
        let dt = time.delta_seconds();

        for (mut velocity, force, mass) in (&mut velocities, &forces, &masses).join() {
            velocity.0 += (force.0 / mass.0) * dt;
        }
    }
}

impl<'s> System<'s> for PhysicsForceResetSystem {
    type SystemData = (WriteStorage<'s, Force>);
    fn run(&mut self, data: Self::SystemData) {
        let mut forces = data;
        for mut force in (&mut forces).join() {
            force.0.x = 0.0;
            force.0.y = 0.0;
        }
    }
}

impl<'s> System<'s> for PhysicsDragSystem {
    type SystemData = (
        WriteStorage<'s, Force>,
        ReadStorage<'s, Velocity>,
        ReadStorage<'s, Drag>,
    );
    fn run(&mut self, data: Self::SystemData) {
        let (mut forces, velocities, drags) = data;

        for (mut force, velocity, drag) in (&mut forces, &velocities, &drags).join() {
            force.0 -= velocity.0 * drag.0
        }
    }
}

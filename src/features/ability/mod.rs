mod abilities;

use self::abilities::{Abil, AbilPlasmaBall, AbilitiesBundle};
use super::components::*;
use super::*;
use amethyst::core::Time;
use input::{BlastAction, BlastInputHandler};
use std::time::Instant;

pub mod abils {
    pub use super::abilities::AbilPlasmaBall;
}

#[derive(Default)]
pub struct AbilityFeature;

impl<'a, 'b> SystemBundle<'a, 'b> for AbilityFeature {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(CastingSystem, "casting", &[]);
        AbilitiesBundle.build(builder)?;
        Ok(())
    }
}

#[derive(Component, Debug, Default, Clone, Serialize, Deserialize)]
#[storage(HashMapStorage)]
pub struct SpellCaster {
    pub abilities: (
        Option<AbilityState>,
        Option<AbilityState>,
        Option<AbilityState>,
        Option<AbilityState>,
    ),
}

#[allow(dead_code)]
pub enum SpellPos {
    Trig1,
    Trig2,
    Trig3,
    Trig4,
}

impl SpellCaster {
    pub fn new() -> Self {
        Self {
            abilities: (None, None, None, None),
        }
    }

    pub fn insert(&mut self, pos: SpellPos, ability: AbilPlasmaBall) {
        match pos {
            SpellPos::Trig1 => self.abilities.0 = Some(AbilityState::new(ability)),
            SpellPos::Trig2 => self.abilities.1 = Some(AbilityState::new(ability)),
            SpellPos::Trig3 => self.abilities.2 = Some(AbilityState::new(ability)),
            SpellPos::Trig4 => self.abilities.3 = Some(AbilityState::new(ability)),
        };
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AbilityState {
    ability: AbilPlasmaBall, // TODO: GENERALIZE THIS
    #[serde(skip_serializing, skip_deserializing, default = "Instant::now")]
    ready_time: Instant,
}

impl AbilityState {
    pub fn new(ability: AbilPlasmaBall) -> Self {
        Self {
            ability,
            ready_time: Instant::now(),
        }
    }
}

// SYSTEMS

pub struct CastingSystem;

impl<'s> System<'s> for CastingSystem {
    type SystemData = (
        WriteStorage<'s, SpellCaster>,
        ReadStorage<'s, Owner>,
        Entities<'s>,
        Read<'s, BlastInputHandler>,
        Read<'s, Time>,
        Read<'s, LazyUpdate>,
    );
    fn run(&mut self, data: Self::SystemData) {
        let (mut casters, owners, entities, input, time, updater) = data;
        let t = time.last_fixed_update();

        for (entity, mut caster, owner) in (&*entities, &mut casters, &owners).join() {
            let mut check_action = |abil: &mut Option<AbilityState>, action: &BlastAction| {
                if let Some(ref mut abil) = abil {
                    let act = input.action_is_down(action).unwrap_or(false);
                    if act && abil.ready_time <= t {
                        abil.ready_time = t + abil.ability.cooldown();
                        abil.ability.apply(entity, &updater);
                    }
                }
            };

            check_action(&mut caster.abilities.0, &BlastAction::Trig1(owner.0));
            check_action(&mut caster.abilities.1, &BlastAction::Trig2(owner.0));
            check_action(&mut caster.abilities.2, &BlastAction::Trig3(owner.0));
            check_action(&mut caster.abilities.3, &BlastAction::Trig4(owner.0));
        }
    }
}

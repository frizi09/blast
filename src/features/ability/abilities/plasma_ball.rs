use super::Abil;
use amethyst::core::cgmath::{InnerSpace, Vector2};
use amethyst::core::{GlobalTransform, Transform};
use blueprint::{Blueprint, Blueprints};
use features::components::{Owner, Velocity};
use features::physics::{Drag, Force, Mass};
use input::{BlastAxis, BlastInputHandler};
use specs::{
    Builder, Entities, Entity, Join, LazyUpdate, NullStorage, Read, ReadExpect, ReadStorage, System,
};
use std::time::Duration;

#[derive(Debug, Component, Default, Clone, Copy, Serialize, Deserialize)]
#[storage(NullStorage)]
pub struct AbilPlasmaBall;

impl Abil for AbilPlasmaBall {
    fn cooldown(&self) -> Duration {
        Duration::from_millis(400)
    }

    fn apply(&self, entity: Entity, updater: &LazyUpdate) {
        updater.insert(entity, self.clone())
    }
}

impl<'s> System<'s> for AbilPlasmaBall {
    type SystemData = (
        Entities<'s>,
        ReadStorage<'s, AbilPlasmaBall>,
        ReadStorage<'s, Transform>,
        ReadStorage<'s, Owner>,
        Read<'s, BlastInputHandler>,
        Read<'s, LazyUpdate>,
        ReadExpect<'s, Blueprints>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (entities, abils, transforms, owners, input, updater, blueprints) = data;

        for (entity, _, transform, owner) in (&*entities, &abils, &transforms, &owners).join() {
            updater.remove::<AbilPlasmaBall>(entity);

            let x = input.axis_value(&BlastAxis::X(owner.0)).unwrap_or(0.0) as f32;
            let y = input.axis_value(&BlastAxis::Y(owner.0)).unwrap_or(0.0) as f32;
            let vel = Vector2::new(x, y);

            let mag = vel.magnitude();
            if mag < 0.01 {
                continue;
            }
            let vel = vel / mag * 500.0;

            blueprints
                .projectile
                .build(updater.create_entity(&entities))
                .with(transform.clone())
                .with(GlobalTransform::default())
                .with(owner.clone())
                .with(Velocity(vel))
                .with(Force::new())
                .with(Mass(1.0))
                .with(Drag(0.9))
                .build();
        }
    }
}

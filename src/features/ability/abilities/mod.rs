use super::super::*;

use specs::{Entity, LazyUpdate};
use std::fmt::Debug;
use std::time::Duration;

pub trait Abil: Send + Sync + Debug {
    fn cooldown(&self) -> Duration;
    fn apply(&self, entity: Entity, updater: &LazyUpdate);
}

mod plasma_ball;

pub use self::plasma_ball::AbilPlasmaBall;

// #[derive(Debug, Clone)]
// pub enum Ability {
//     PlasmaBall(AbilPlasmaBall),
// }

#[derive(Default)]
pub struct AbilitiesBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for AbilitiesBundle {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(AbilPlasmaBall, "abil_plasma_ball", &[]);
        Ok(())
    }
}

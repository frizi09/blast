impl<'a, 'b> SystemBundle<'a, 'b> for MovementFeature {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(MovementInputSystem, "movement_input", &["physics_force"]);
        builder.add(
            MovementAnimationSystem,
            "movement_animation",
            &["animation_control"],
        );
        Ok(())
    }
}

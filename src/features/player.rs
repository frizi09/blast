use super::*;

// COMPONENTS
#[derive(Component, Debug, Default, Serialize, Deserialize)]
#[storage(NullStorage)]
pub struct PlayerController;

#[derive(Component, Debug, Clone, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Owner(pub usize);

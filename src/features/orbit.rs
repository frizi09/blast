use super::*;
use amethyst::core::timing::Time;
use amethyst::core::Transform;

#[derive(Default)]
pub struct OrbitFeature;
impl<'a, 'b> SystemBundle<'a, 'b> for OrbitFeature {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(OrbitSystem, "orbit", &[]);
        Ok(())
    }
}

#[derive(Component, Debug, Clone, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Orbit {
    pub radius: f32,
    pub angle: f32,
    pub speed: f32,
}

impl Orbit {
    pub fn new(radius: f32, angle: f32, speed: f32) -> Self {
        Self {
            radius,
            angle,
            speed,
        }
    }
}

pub struct OrbitSystem;
impl<'s> System<'s> for OrbitSystem {
    type SystemData = (
        Read<'s, Time>,
        WriteStorage<'s, Transform>,
        WriteStorage<'s, Orbit>,
    );
    fn run(&mut self, data: Self::SystemData) {
        let (time, mut transform, mut orbit) = data;
        let dt = time.delta_seconds();

        for (mut transform, mut orbit) in (&mut transform, &mut orbit).join() {
            orbit.angle += orbit.speed * dt;
            transform.translation[0] = orbit.angle.cos() * orbit.radius;
            transform.translation[1] = orbit.angle.sin() * orbit.radius;
        }
    }
}

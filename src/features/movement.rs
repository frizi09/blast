use amethyst::animation::{AnimationCommand, EndControl};
use amethyst::core::cgmath::{InnerSpace, Vector2};
use amethyst::core::timing::Time;
use amethyst::core::Transform;
use input::{BlastAxis, BlastInputHandler};
use player_anim::PlayerAnimControlSet;
use player_anim::{Direction, PlayerAnimation, PlayerAnimations};

use super::components::*;
use super::*;

#[derive(Default)]
pub struct MovementFeature;

impl<'a, 'b> SystemBundle<'a, 'b> for MovementFeature {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(MovementInputSystem, "movement_input", &["physics_force"]);
        builder.add(
            MovementAnimationSystem,
            "movement_animation",
            &["animation_control"],
        );
        Ok(())
    }
}

#[derive(Component, Debug, Serialize, Deserialize)]
#[storage(HashMapStorage)]
pub struct MovementInertia {
    pub vel: Vector2<f32>,
}

impl MovementInertia {
    pub fn new() -> Self {
        Self {
            vel: Vector2::new(0.0, 0.0),
        }
    }
}

const MOVE_ACCELERATION: f32 = 4000.0;
const TARGET_SPEED: f32 = 150.0;
const DEAD_ZONE: f32 = 0.01;
// SYSTEMS

struct MovementInputSystem;
struct MovementAnimationSystem;

impl<'s> System<'s> for MovementInputSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        WriteStorage<'s, MovementInertia>,
        ReadStorage<'s, PlayerController>,
        ReadStorage<'s, Owner>,
        Read<'s, BlastInputHandler>,
        Read<'s, Time>,
    );
    fn run(&mut self, data: Self::SystemData) {
        let (mut transforms, mut inertias, _, owner, input, time) = data;
        let dt = time.delta_seconds();

        let acc_per_frame = MOVE_ACCELERATION * dt;

        for (mut transform, mut inertia, owner) in (&mut transforms, &mut inertias, &owner).join() {
            let x_axis = BlastAxis::X(owner.0);
            let y_axis = BlastAxis::Y(owner.0);

            let x = input.axis_value(&x_axis).unwrap_or(0.0) as f32;
            let y = input.axis_value(&y_axis).unwrap_or(0.0) as f32;
            let is_moving = x > DEAD_ZONE || -x > DEAD_ZONE || y > DEAD_ZONE || -y > DEAD_ZONE;

            let target_vel = if is_moving {
                let input_vec = Vector2::new(x, y);
                let magnitude = input_vec.magnitude();
                let scale = (magnitude.min(1.0) * TARGET_SPEED) / magnitude;
                input_vec * scale
            } else {
                Vector2::new(0.0, 0.0)
            };

            let delta_vel = target_vel - inertia.vel;
            let delta_mag = delta_vel.magnitude();
            if delta_mag > 0.0000001 {
                let desired_mag = delta_mag.min(acc_per_frame);

                let delta_tensor = delta_vel / delta_mag;
                let frame_acc = delta_tensor * desired_mag;
                let new_vel = inertia.vel + frame_acc;
                inertia.vel = new_vel;
            }

            transform.translation[0] += inertia.vel.x * dt;
            transform.translation[1] += inertia.vel.y * dt;
        }
    }
}

fn anim_for_vector(vec: Vector2<f32>, prev_animation: Option<PlayerAnimation>) -> PlayerAnimation {
    let (x, y) = (vec.x, vec.y);
    let (abs_x, abs_y) = (x.abs(), y.abs());

    let max = abs_x.max(abs_y);
    if max < 0.1 {
        let dir = prev_animation
            .map(|a| match a {
                PlayerAnimation::Idle(d) => d,
                PlayerAnimation::Walking(d) => d,
            })
            .unwrap_or(Direction::Bottom);
        PlayerAnimation::Idle(dir)
    } else if abs_x >= abs_y {
        if x > 0.0 {
            PlayerAnimation::Walking(Direction::Right)
        } else {
            PlayerAnimation::Walking(Direction::Left)
        }
    } else {
        if y > 0.0 {
            PlayerAnimation::Walking(Direction::Top)
        } else {
            PlayerAnimation::Walking(Direction::Bottom)
        }
    }
}

fn apply_animation(
    anim_defs: &PlayerAnimations,
    anim: PlayerAnimation,
    anim_controller: &mut PlayerAnimControlSet,
) {
    anim_controller.add_animation(
        anim,
        anim_defs.get(&anim).unwrap(),
        EndControl::Loop(None),
        1.0,
        AnimationCommand::Start,
    );

    for (id, ref mut control) in anim_controller.animations.iter_mut() {
        if *id != anim {
            control.command = AnimationCommand::Abort;
        }
    }
}

impl<'s> System<'s> for MovementAnimationSystem {
    type SystemData = (
        WriteStorage<'s, PlayerAnimControlSet>,
        ReadStorage<'s, MovementInertia>,
        Read<'s, PlayerAnimations>,
    );
    fn run(&mut self, data: Self::SystemData) {
        let (mut anims, inertias, anim_defs) = data;

        for (mut anim_controller, inertia) in (&mut anims, &inertias).join() {
            let prev_animation = anim_controller.animations.get(0).map(|a| a.0);
            let desired_anim = anim_for_vector(inertia.vel, prev_animation);
            apply_animation(&anim_defs, desired_anim, anim_controller);
        }
    }
}

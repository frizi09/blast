mod ability;
pub mod movement;
mod orbit;
mod physics;
mod player;
mod tilemap;

pub use self::ability::SpellPos;
pub use amethyst::core::{Result, SystemBundle};
pub use amethyst::ecs::prelude::*;
pub use amethyst::ecs::{DispatcherBuilder, Join};

pub mod components {
    pub use super::ability::SpellCaster;
    pub use super::movement::MovementInertia;
    pub use super::orbit::Orbit;
    pub use super::physics::{Drag, Force, Velocity};
    pub use super::player::{Owner, PlayerController};
    pub use super::tilemap::Tilemap;
}

pub mod abilities {
    pub use super::ability::abils::*;
}

#[derive(Default)]
pub struct BlastBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for BlastBundle {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        physics::PhysicsFeature.build(builder)?;
        movement::MovementFeature.build(builder)?;
        orbit::OrbitFeature.build(builder)?;
        tilemap::TilemapFeature.build(builder)?;
        ability::AbilityFeature.build(builder)?;
        Ok(())
    }
}

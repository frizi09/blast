use super::*;
use amethyst::assets::{AssetStorage, Handle, Loader};
use amethyst::renderer::Mesh;
use amethyst::renderer::PosTex;

pub struct TilemapFeature;

impl<'a, 'b> SystemBundle<'a, 'b> for TilemapFeature {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(TilemapMeshGeneratorSystem, "", &[]);
        Ok(())
    }
}

// enum TileType {
//     Ground,
//     Lava,
// }

// struct Map {
//     width: u32,
//     height: u32,
//     tiles: Vec<TileType>,
// }

#[derive(Component, Debug, Serialize, Deserialize)]
pub struct Tilemap {
    pub num_cols: u32,
    pub tiles: Vec<u32>, // tile is constant size (64x32)
}

#[derive(Default)]
struct TilemapMeshGeneratorSystem;

impl<'s> System<'s> for TilemapMeshGeneratorSystem {
    type SystemData = (
        Entities<'s>,
        ReadStorage<'s, Tilemap>,
        ReadStorage<'s, Handle<Mesh>>,
        ReadExpect<'s, Loader>,
        Read<'s, AssetStorage<Mesh>>,
        Read<'s, LazyUpdate>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (entities, tilemaps, mesh, loader, mesh_storage, lazy) = data;

        for (entity, tilemap, _not_mesh) in (&*entities, &tilemaps, !&mesh).join() {
            let generated_mesh =
                loader.load_from_data(generate_mesh(tilemap).into(), (), &mesh_storage);
            lazy.insert(entity, generated_mesh);
        }
    }
}

const TILE_WIDTH: u32 = 64;
const TILE_HEIGHT: u32 = 32;

const TILE_WORLD_WIDTH: u32 = TILE_WIDTH;
const TILE_WORLD_HEIGHT: u32 = TILE_HEIGHT - 1;

const TILEMAP_COLUMNS: u32 = 2;
const TILEMAP_ROWS: u32 = 8;

const TILEMAP_IMG_WIDTH: u32 = TILE_WIDTH * TILEMAP_COLUMNS;
const TILEMAP_IMG_HEIGHT: u32 = TILE_HEIGHT * TILEMAP_ROWS / 2;

fn normalize_texcoords(pixel_coords: (u32, u32)) -> (f32, f32) {
    (
        pixel_coords.0 as f32 / TILEMAP_IMG_WIDTH as f32,
        pixel_coords.1 as f32 / TILEMAP_IMG_HEIGHT as f32,
    )
}

fn generate_mesh(tilemap: &Tilemap) -> Vec<PosTex> {
    /* For 4 columns and 3 rows
     * 0 1 2 3
     *  4 5 6 7
     * 8 9 A B
     */

    let mut vertices = Vec::with_capacity(tilemap.tiles.len());

    for (i, tile_idx) in tilemap.tiles.iter().enumerate() {
        // SPRITE TEXCOORDS
        let sprite_tile_x = tile_idx % TILEMAP_COLUMNS;
        let sprite_tile_y = tile_idx / TILEMAP_COLUMNS;

        let sprite_img_x = sprite_tile_x * TILE_WIDTH;
        let sprite_img_y = sprite_tile_y * TILE_HEIGHT;

        let sprite_img_topleft = (sprite_img_x, sprite_img_y);
        let sprite_img_topright = (sprite_img_x + TILE_WIDTH, sprite_img_y);
        let sprite_img_bottomleft = (sprite_img_x, sprite_img_y + TILE_HEIGHT);
        let sprite_img_bottomright = (sprite_img_x + TILE_WIDTH, sprite_img_y + TILE_HEIGHT);

        let norm_topleft = normalize_texcoords(sprite_img_topleft);
        let norm_topright = normalize_texcoords(sprite_img_topright);
        let norm_bottomleft = normalize_texcoords(sprite_img_bottomleft);
        let norm_bottomright = normalize_texcoords(sprite_img_bottomright);

        // SCREEN SPACE POSITION
        let screen_tile_x = i as u32 % tilemap.num_cols;
        let screen_tile_y = i as u32 / tilemap.num_cols;

        let screen_even_row = screen_tile_y % 2 == 0;
        let screen_offset_x = if screen_even_row { 0 } else { TILE_WIDTH / 2 };

        let screen_x = screen_tile_x * TILE_WIDTH + screen_offset_x;
        let screen_y = screen_tile_y * TILE_HEIGHT / 2;

        let screen_topleft = (screen_x, screen_y);
        let screen_topright = (screen_x + TILE_WIDTH, screen_y);
        let screen_bottomleft = (screen_x, screen_y + TILE_HEIGHT);
        let screen_bottomright = (screen_x + TILE_WIDTH, screen_y + TILE_HEIGHT);

        // create quad
        vertices.push(create_postex(screen_bottomleft, norm_bottomleft));
        vertices.push(create_postex(screen_topleft, norm_topleft));
        vertices.push(create_postex(screen_topright, norm_topright));

        vertices.push(create_postex(screen_bottomleft, norm_bottomleft));
        vertices.push(create_postex(screen_topright, norm_topright));
        vertices.push(create_postex(screen_bottomright, norm_bottomright));
    }
    vertices
}

fn pos_to_3f(tuple: (u32, u32)) -> [f32; 3] {
    [tuple.0 as f32, tuple.1 as f32, 0.]
}

fn tex_to_2f(tuple: (f32, f32)) -> [f32; 2] {
    [tuple.0, tuple.1]
}

fn create_postex(pos: (u32, u32), tex: (f32, f32)) -> PosTex {
    PosTex {
        position: pos_to_3f(pos),
        tex_coord: tex_to_2f(tex),
    }
}

use amethyst::assets::{AssetStorage, Loader};
use amethyst::prelude::*;
use amethyst::renderer::{PngFormat, Texture, TextureHandle, TextureMetadata};
use gfx::format::ChannelType;

/// Returns a `TextureHandle` to the image.
///
/// # Parameters
///
/// * `name`: Path to the sprite sheet.
/// * `world`: `World` that stores resources.
pub fn load<N>(name: N, world: &World) -> TextureHandle
where
    N: Into<String>,
{
    let loader = world.read_resource::<Loader>();
    loader.load(
        name,
        PngFormat,
        TextureMetadata::default().with_channel(ChannelType::Srgb),
        (),
        &world.read_resource::<AssetStorage<Texture>>(),
    )
}

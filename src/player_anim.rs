use amethyst::animation::{Animation, AnimationBundle, AnimationControlSet};
use amethyst::assets::Handle;
use amethyst::ecs::World;
use amethyst::renderer::Material;
use sprite_animation;
use std::collections::HashMap;

pub type PlayerAnimControlSet = AnimationControlSet<PlayerAnimation, Material>;
pub type PlayerAnimBundle<'a> = AnimationBundle<'a, PlayerAnimation, Material>;

#[derive(Clone, Copy, Eq, PartialEq, Hash)]
pub enum Direction {
  Top,
  Left,
  Right,
  Bottom,
}

#[derive(Clone, Copy, Eq, PartialEq, Hash)]
pub enum PlayerAnimation {
  Walking(Direction),
  Idle(Direction),
}

pub type PlayerAnimations = HashMap<PlayerAnimation, Handle<Animation<Material>>>;

pub fn prepare_player_animation(world: &mut World) {
  let frame_time = 0.07;
  let anim_top = sprite_animation::prepare_animation(0, 105..114, frame_time, world);
  let anim_left = sprite_animation::prepare_animation(0, 118..127, frame_time, world);
  let anim_bottom = sprite_animation::prepare_animation(0, 131..140, frame_time, world);
  let anim_right = sprite_animation::prepare_animation(0, 144..153, frame_time, world);

  let anim_top_idle =
    sprite_animation::prepare_animation(0, vec![104].into_iter(), frame_time, world);
  let anim_left_idle =
    sprite_animation::prepare_animation(0, vec![117].into_iter(), frame_time, world);
  let anim_bottom_idle =
    sprite_animation::prepare_animation(0, vec![130].into_iter(), frame_time, world);
  let anim_right_idle =
    sprite_animation::prepare_animation(0, vec![143].into_iter(), frame_time, world);

  let mut animations = PlayerAnimations::new();
  animations.insert(PlayerAnimation::Walking(Direction::Top), anim_top);
  animations.insert(PlayerAnimation::Walking(Direction::Left), anim_left);
  animations.insert(PlayerAnimation::Walking(Direction::Bottom), anim_bottom);
  animations.insert(PlayerAnimation::Walking(Direction::Right), anim_right);
  animations.insert(PlayerAnimation::Idle(Direction::Top), anim_top_idle);
  animations.insert(PlayerAnimation::Idle(Direction::Left), anim_left_idle);
  animations.insert(PlayerAnimation::Idle(Direction::Bottom), anim_bottom_idle);
  animations.insert(PlayerAnimation::Idle(Direction::Right), anim_right_idle);
  world.add_resource(animations);
}

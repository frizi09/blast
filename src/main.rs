#[macro_use]
extern crate log;
extern crate amethyst;
extern crate fern;
#[macro_use]
extern crate imgui;
extern crate imgui_gfx_renderer;
extern crate palette;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate specs;
#[macro_use]
extern crate specs_derive;
extern crate gfx;
extern crate hibitset;

mod amethyst_imgui;
mod blast;
mod blueprint;
mod features;
mod game_config;
mod gamma_pass;
mod input;
mod inspector;
mod player_anim;
mod png_loader;
mod sprite;
mod sprite_animation;
mod sprite_sheet_loader;

use amethyst::Result;

fn run() -> Result<()> {
    use amethyst::config::Config;
    use amethyst::Application;
    use blast::{setup_game_data, Blast};
    use game_config::GameConfig;

    setup_logging();

    let game_config = GameConfig::load(&"./resources/config.ron");

    let game_data = setup_game_data(game_config)?;
    let mut game = Application::new("./", Blast::new(), game_data)?;
    game.run();
    Ok(())
}

fn setup_logging() {
    use amethyst::config::Config;
    use fern::Dispatch;
    use game_config::LoggingConfig;

    let log_config: Vec<LoggingConfig> = Config::load(&"./resources/logging_config.ron");

    let log_base = Dispatch::new().format(|out, message, record| {
        out.finish(format_args!(
            "[{}][{}] {}",
            record.level(),
            record.target(),
            message,
        ))
    });

    let log_dispatch = log_config.iter().fold(log_base, |base, config| {
        base.chain::<Dispatch>(config.into())
    });

    log_dispatch.apply().unwrap(); // TODO: use failure
}

fn main() {
    if let Err(e) = run() {
        println!("Error occurred during game execution: {}", e);
        ::std::process::exit(1);
    }
}

use amethyst::animation::{
    Animation, InterpolationFunction, MaterialChannel, MaterialPrimitive, Sampler,
};
use amethyst::assets::{Handle, Loader};
use amethyst::prelude::*;
use amethyst::renderer::Material;
use blast::SpritesheetSet;

pub fn prepare_animation<I: Iterator<Item = usize>>(
    sprite_sheet_id: u64,
    frames: I,
    frame_time: f32,
    world: &mut World,
) -> Handle<Animation<Material>> {
    let (sprite_offsets, texture_id) = {
        let sprite_sheet_res = world.read_resource::<SpritesheetSet>();
        let sprite_sheet = sprite_sheet_res.get(&sprite_sheet_id).unwrap();

        let sprite_offsets = frames
            .map(|frame| sprite_sheet.sprites[frame].clone().into())
            .collect::<Vec<MaterialPrimitive>>();
        (sprite_offsets, sprite_sheet.texture_id)
    };

    let len = sprite_offsets.len();

    let sprite_offset_sampler = {
        Sampler {
            input: (0..len).map(|i| i as f32 * frame_time).collect(),
            function: InterpolationFunction::Step,
            output: sprite_offsets,
        }
    };

    let texture_sampler = Sampler {
        input: vec![0.],
        function: InterpolationFunction::Step,
        output: vec![MaterialPrimitive::Texture(texture_id)],
    };

    let loader = world.write_resource::<Loader>();
    let sampler_animation_handle =
        loader.load_from_data(sprite_offset_sampler, (), &world.read_resource());
    let texture_animation_handle =
        loader.load_from_data(texture_sampler, (), &world.read_resource());

    let animation = Animation {
        nodes: vec![
            (0, MaterialChannel::AlbedoTexture, texture_animation_handle),
            (0, MaterialChannel::AlbedoOffset, sampler_animation_handle),
        ],
    };
    loader.load_from_data(animation, (), &world.read_resource())
}

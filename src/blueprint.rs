use amethyst::renderer::{Material, MeshHandle};
use specs::world::LazyBuilder;
use specs::Builder;

pub trait Blueprint {
    fn build<'a>(&self, builder: LazyBuilder<'a>) -> LazyBuilder<'a>;
}

pub struct Blueprints {
    pub projectile: ProjectileBlueprint,
}

pub struct ProjectileBlueprint {
    pub mesh: MeshHandle,
    pub material: Material,
}

impl Blueprint for ProjectileBlueprint {
    fn build<'a>(&self, builder: LazyBuilder<'a>) -> LazyBuilder<'a> {
        builder.with(self.mesh.clone()).with(self.material.clone())
    }
}

use amethyst::input::{Bindings, InputBundle, InputHandler};

pub type BlastBindings = Bindings<BlastAxis, BlastAction>;
pub type BlastInputBundle = InputBundle<BlastAxis, BlastAction>;
pub type BlastInputHandler = InputHandler<BlastAxis, BlastAction>;

#[derive(Debug, Hash, Eq, PartialEq, Serialize, Deserialize, Clone, Copy)]
pub enum BlastAxis {
    X(usize),
    Y(usize),
}

#[derive(Debug, Hash, Eq, PartialEq, Serialize, Deserialize, Clone, Copy)]
pub enum BlastAction {
    Trig1(usize),
    Trig2(usize),
    Trig3(usize),
    Trig4(usize),
}

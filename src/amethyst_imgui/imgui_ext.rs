use imgui::{sys, ImStr, ImVec2, StyleVar, Ui};

pub trait UiExt {
    fn indent(&self, w: f32);
    fn unindent(&self, w: f32);
    fn begin_group(&self);
    fn end_group(&self);
    fn scroll_frame<'p>(&self, name: &'p ImStr, size: impl Into<ImVec2>, f: impl FnOnce());
}

impl<'a> UiExt for Ui<'a> {
    fn indent(&self, w: f32) {
        unsafe {
            sys::igIndent(w);
        }
    }

    fn unindent(&self, w: f32) {
        unsafe {
            sys::igUnindent(w);
        }
    }
    fn begin_group(&self) {
        unsafe {
            sys::igBeginGroup();
        }
    }
    fn end_group(&self) {
        unsafe {
            sys::igEndGroup();
        }
    }

    fn scroll_frame<'p>(&self, name: &'p ImStr, size: impl Into<ImVec2>, f: impl FnOnce()) {
        self.with_style_vars(&[StyleVar::WindowPadding((8.0, 4.0).into())], || {
            self.child_frame(name, size)
                .show_borders(true)
                .always_show_vertical_scroll_bar(true)
                .build(|| self.with_style_vars(&[StyleVar::WindowPadding((4.0, 4.0).into())], f))
        })
    }
}

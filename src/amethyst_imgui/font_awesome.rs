pub const GLYPH_RANGE: &[u16] = &[0xf000, 0xf63c, 0x0];

pub const ICON_FA_EXTERNAL_LINK_ALT: &str = "\u{f35d}";

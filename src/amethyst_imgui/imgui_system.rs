use amethyst::core::shrev::{EventChannel, ReaderId};
use amethyst::core::Time;
use amethyst::renderer::ScreenDimensions;
use amethyst::winit::{
    DeviceEvent, ElementState, Event, MouseButton, MouseScrollDelta, VirtualKeyCode, WindowEvent,
};
use imgui::ImGui;
use specs::{Read, ReadExpect, Resources, RunNow, SystemData, WriteExpect};

pub struct ImguiSystem {
    reader: Option<ReaderId<Event>>,
    bootstrapped: bool,
    mouse_state: [bool; 5],
}

impl ImguiSystem {
    pub fn new() -> Self {
        ImguiSystem {
            reader: None,
            bootstrapped: false,
            mouse_state: [false; 5],
        }
    }

    fn process_event(&mut self, event: &Event, imgui: &mut ImGui) {
        let scale = imgui.display_framebuffer_scale();

        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput { input, .. } => {
                    if let Some(key) = input.virtual_keycode.and_then(keycode_to_imgui) {
                        imgui.set_key(key, input.state == ElementState::Pressed)
                    }
                }
                WindowEvent::ReceivedCharacter(input) => {
                    imgui.add_input_character(*input);
                }
                WindowEvent::MouseInput { button, state, .. } => {
                    if let Some(button_id) = mouse_button_to_imgui(*button) {
                        self.mouse_state[button_id] = *state == ElementState::Pressed;
                    }
                }
                WindowEvent::CursorMoved { position, .. } => {
                    imgui.set_mouse_pos(
                        (position.0 as f32) / scale.0,
                        (position.1 as f32) / scale.1,
                    );
                }
                _ => {}
            },
            Event::DeviceEvent { event, .. } => match event {
                DeviceEvent::MouseWheel { delta } => {
                    let pixels_delta = match delta {
                        MouseScrollDelta::LineDelta(_, y_lines) => *y_lines * 15.0,
                        MouseScrollDelta::PixelDelta(_, y) => *y,
                    } * 0.05;
                    let scaled_delta = pixels_delta / scale.1;
                    let current_delta = imgui.mouse_wheel();
                    imgui.set_mouse_wheel(current_delta + scaled_delta);
                }
                _ => {}
            },
            _ => {}
        }
    }
}

type InspectorData<'a> = (
    Read<'a, EventChannel<Event>>,
    WriteExpect<'a, ImGui>,
    ReadExpect<'a, ScreenDimensions>,
    ReadExpect<'a, Time>,
);

impl<'a> RunNow<'a> for ImguiSystem {
    fn setup(&mut self, res: &mut Resources) {
        use imgui::ImString;
        let mut imgui = ImGui::init();

        imgui.set_ini_filename(Some(ImString::new("imgui.ini")));
        configure_keys(&mut imgui);
        res.insert(imgui);

        self.reader = Some(res.fetch_mut::<EventChannel<Event>>().register_reader());
    }

    fn run_now(&mut self, res: &'a Resources) {
        let (input, mut imgui, screen_dimensions, time) = InspectorData::fetch(res);

        // pass single frame so renderer is initialized first
        if !self.bootstrapped {
            self.bootstrapped = true;
            return;
        }

        for event in input.read(&mut self.reader.as_mut().unwrap()) {
            self.process_event(event, &mut imgui);
        }
        imgui.set_mouse_down(&self.mouse_state);

        let size_pixels = (
            screen_dimensions.width() as u32,
            screen_dimensions.height() as u32,
        );
        let hdipi = screen_dimensions.hidpi_factor();
        let size_points = (
            (size_pixels.0 as f32 / hdipi) as u32,
            (size_pixels.1 as f32 / hdipi) as u32,
        );

        let ui = imgui.frame(size_points, size_pixels, time.delta_seconds());
    }
}

fn configure_keys(imgui: &mut ImGui) {
    use imgui::ImGuiKey;

    imgui.set_imgui_key(ImGuiKey::Tab, 0);
    imgui.set_imgui_key(ImGuiKey::LeftArrow, 1);
    imgui.set_imgui_key(ImGuiKey::RightArrow, 2);
    imgui.set_imgui_key(ImGuiKey::UpArrow, 3);
    imgui.set_imgui_key(ImGuiKey::DownArrow, 4);
    imgui.set_imgui_key(ImGuiKey::PageUp, 5);
    imgui.set_imgui_key(ImGuiKey::PageDown, 6);
    imgui.set_imgui_key(ImGuiKey::Home, 7);
    imgui.set_imgui_key(ImGuiKey::End, 8);
    imgui.set_imgui_key(ImGuiKey::Delete, 9);
    imgui.set_imgui_key(ImGuiKey::Backspace, 10);
    imgui.set_imgui_key(ImGuiKey::Enter, 11);
    imgui.set_imgui_key(ImGuiKey::Escape, 12);
    imgui.set_imgui_key(ImGuiKey::A, 13);
    imgui.set_imgui_key(ImGuiKey::C, 14);
    imgui.set_imgui_key(ImGuiKey::V, 15);
    imgui.set_imgui_key(ImGuiKey::X, 16);
    imgui.set_imgui_key(ImGuiKey::Y, 17);
    imgui.set_imgui_key(ImGuiKey::Z, 18);
}

fn keycode_to_imgui(key: VirtualKeyCode) -> Option<u8> {
    match key {
        VirtualKeyCode::Tab => Some(0),
        VirtualKeyCode::Left => Some(1),
        VirtualKeyCode::Right => Some(2),
        VirtualKeyCode::Up => Some(3),
        VirtualKeyCode::Down => Some(4),
        VirtualKeyCode::PageUp => Some(5),
        VirtualKeyCode::PageDown => Some(6),
        VirtualKeyCode::Home => Some(7),
        VirtualKeyCode::End => Some(8),
        VirtualKeyCode::Delete => Some(9),
        VirtualKeyCode::Back => Some(10),
        VirtualKeyCode::Return => Some(11),
        VirtualKeyCode::Escape => Some(12),
        VirtualKeyCode::A => Some(13),
        VirtualKeyCode::C => Some(14),
        VirtualKeyCode::V => Some(15),
        VirtualKeyCode::X => Some(16),
        VirtualKeyCode::Y => Some(17),
        VirtualKeyCode::Z => Some(18),
        _ => None,
    }
}

fn mouse_button_to_imgui(button: MouseButton) -> Option<usize> {
    match button {
        MouseButton::Left => Some(0),
        MouseButton::Right => Some(1),
        MouseButton::Middle => Some(2),
        MouseButton::Other(0) => Some(3),
        MouseButton::Other(1) => Some(4),
        _ => None,
    }
}

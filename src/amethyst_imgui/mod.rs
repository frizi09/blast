use amethyst::core::{Result, SystemBundle};
use imgui::Ui;
use specs::DispatcherBuilder;

mod font_awesome;
mod imgui_ext;
mod imgui_pass;
mod imgui_system;

pub use self::imgui_ext::UiExt;
pub use self::imgui_pass::ImguiPass;
pub use self::imgui_system::ImguiSystem;

pub struct ImguiAccess;
impl ImguiAccess {
    pub fn ui<'a>(&self) -> Option<&Ui<'a>> {
        unsafe { Ui::current_ui() }
    }
}

#[derive(Default)]
pub struct ImguiBundle;
impl<'a, 'b> SystemBundle<'a, 'b> for ImguiBundle {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add_thread_local(ImguiSystem::new());
        Ok(())
    }
}

use super::font_awesome::GLYPH_RANGE;
use amethyst::renderer::error::Result;
use amethyst::renderer::pipe::pass::{Pass, PassData};
use amethyst::renderer::{Effect, Encoder, Factory, NewEffect, Resources};
use imgui::{FontGlyphRange, ImFontConfig, ImGui, ImGuiCol, ImGuiStyle, ImVec4, Ui};
use imgui_gfx_renderer::{Renderer, Shaders};
use specs::WriteExpect;

static VERT_SRC: &[u8] = include_bytes!("shaders/vertex.glsl");
static FRAG_SRC: &[u8] = include_bytes!("shaders/frag.glsl");

pub struct ImguiPass {
    imgui_renderer: Option<Renderer<Resources>>,
}

impl ImguiPass {
    pub fn new() -> Self {
        ImguiPass {
            imgui_renderer: None,
        }
    }
}

impl<'a> PassData<'a> for ImguiPass {
    type Data = (WriteExpect<'a, ImGui>,);
}

impl Pass for ImguiPass {
    fn compile(&mut self, effect: NewEffect) -> Result<Effect> {
        effect.simple(VERT_SRC, FRAG_SRC).build()
    }

    fn apply<'a, 'b: 'a>(
        &'a mut self,
        encoder: &mut Encoder,
        effect: &mut Effect,
        mut factory: Factory,
        (mut imgui,): <Self as PassData>::Data,
    ) {
        if let Some(ref mut renderer) = self.imgui_renderer {
            renderer.update_render_target(effect.data.out_blends[0].clone())
        } else {
            let config = ImFontConfig::new()
                .oversample_h(2)
                .oversample_v(2)
                .pixel_snap_h(true)
                .size_pixels(12.0)
                .rasterizer_multiply(2.0);

            config.add_font(
                &mut imgui.fonts(),
                include_bytes!("fonts/Montserrat-Regular.ttf"),
                &FontGlyphRange::default(),
            );
            config.merge_mode(true).add_font(
                &mut imgui.fonts(),
                include_bytes!("fonts/fa-solid-900.ttf"),
                &FontGlyphRange::from_slice(&GLYPH_RANGE),
            );

            {
                let style = imgui.style_mut();
                measures_metro(style);
                style_rcc(style, true, 0.65);

                fn imgui_gamma_to_linear(col: ImVec4) -> ImVec4 {
                    let x = col.x.powf(2.2);
                    let y = col.y.powf(2.2);
                    let z = col.z.powf(2.2);
                    let w = 1.0 - (1.0 - col.w).powf(2.2);
                    ImVec4::new(x, y, z, w)
                }

                for col in 0..style.colors.len() {
                    style.colors[col] = imgui_gamma_to_linear(style.colors[col]);
                }
            }

            self.imgui_renderer = Some(
                Renderer::init(
                    &mut imgui,
                    &mut factory,
                    Shaders::GlSl400,
                    effect.data.out_blends[0].clone(),
                ).expect("Failed to initialize renderer"),
            );
        }

        if let Some(ui) = unsafe { Ui::current_ui() } {
            let ui: Ui<'static> = unsafe {
                use std::mem;
                mem::transmute(ui)
            };

            if let Some(ref mut renderer) = self.imgui_renderer {
                renderer
                    .render(ui, &mut factory, encoder)
                    .expect("Rendering failed");
            }
        }
    }
}

fn measures_metro(style: &mut ImGuiStyle) {
    style.anti_aliased_fill = false;
    style.window_padding = (4.0, 4.0).into();
    style.window_min_size = (32.0, 32.0).into();
    style.frame_padding = (4.0, 2.0).into();
    style.item_spacing = (8.0, 4.0).into();
    style.item_inner_spacing = (4.0, 4.0).into();

    style.indent_spacing = 21.0;
    style.columns_min_spacing = 3.0;
    style.scrollbar_size = 12.0;
    style.window_rounding = 0.0;
    style.frame_rounding = 1.0;
    style.grab_rounding = 1.0;
    style.display_window_padding = (22.0, 22.0).into();
    style.display_safe_area_padding = (4.0, 4.0).into();
    style.anti_aliased_lines = true;
    style.curve_tessellation_tol = 1.25;
}

fn style_rcc(style: &mut ImGuiStyle, dark: bool, alpha: f32) {
    let c = &mut style.colors;

    // light style from Pacôme Danhiez (user itamago) https://github.com/ocornut/imgui/pull/511#issuecomment-175719267
    // style.alpha = 1.0;
    // style.frame_rounding = 3.0;
    c[ImGuiCol::Text as usize] = (0.00, 0.00, 0.00, 1.00).into();
    c[ImGuiCol::TextDisabled as usize] = (0.60, 0.60, 0.60, 1.00).into();
    c[ImGuiCol::WindowBg as usize] = (0.94, 0.94, 0.94, 0.94).into();
    c[ImGuiCol::PopupBg as usize] = (1.00, 1.00, 1.00, 0.94).into();
    c[ImGuiCol::Border as usize] = (0.00, 0.00, 0.00, 0.39).into();
    c[ImGuiCol::BorderShadow as usize] = (1.00, 1.00, 1.00, 0.10).into();
    c[ImGuiCol::FrameBg as usize] = (1.00, 1.00, 1.00, 0.94).into();
    c[ImGuiCol::FrameBgHovered as usize] = (0.26, 0.59, 0.98, 0.40).into();
    c[ImGuiCol::FrameBgActive as usize] = (0.26, 0.59, 0.98, 0.67).into();
    c[ImGuiCol::TitleBg as usize] = (0.96, 0.96, 0.96, 1.00).into();
    c[ImGuiCol::TitleBgCollapsed as usize] = (1.00, 1.00, 1.00, 0.51).into();
    c[ImGuiCol::TitleBgActive as usize] = (0.82, 0.82, 0.82, 1.00).into();
    c[ImGuiCol::MenuBarBg as usize] = (0.86, 0.86, 0.86, 1.00).into();
    c[ImGuiCol::ScrollbarBg as usize] = (0.98, 0.98, 0.98, 0.53).into();
    c[ImGuiCol::ScrollbarGrab as usize] = (0.69, 0.69, 0.69, 1.00).into();
    c[ImGuiCol::ScrollbarGrabHovered as usize] = (0.59, 0.59, 0.59, 1.00).into();
    c[ImGuiCol::ScrollbarGrabActive as usize] = (0.49, 0.49, 0.49, 1.00).into();
    c[ImGuiCol::CheckMark as usize] = (0.26, 0.59, 0.98, 1.00).into();
    c[ImGuiCol::SliderGrab as usize] = (0.24, 0.52, 0.88, 1.00).into();
    c[ImGuiCol::SliderGrabActive as usize] = (0.26, 0.59, 0.98, 1.00).into();
    c[ImGuiCol::Button as usize] = (0.26, 0.59, 0.98, 0.40).into();
    c[ImGuiCol::ButtonHovered as usize] = (0.26, 0.59, 0.98, 1.00).into();
    c[ImGuiCol::ButtonActive as usize] = (0.06, 0.53, 0.98, 1.00).into();
    c[ImGuiCol::Header as usize] = (0.26, 0.59, 0.98, 0.31).into();
    c[ImGuiCol::HeaderHovered as usize] = (0.26, 0.59, 0.98, 0.80).into();
    c[ImGuiCol::HeaderActive as usize] = (0.26, 0.59, 0.98, 1.00).into();
    c[ImGuiCol::ResizeGrip as usize] = (1.00, 1.00, 1.00, 0.50).into();
    c[ImGuiCol::ResizeGripHovered as usize] = (0.26, 0.59, 0.98, 0.67).into();
    c[ImGuiCol::ResizeGripActive as usize] = (0.26, 0.59, 0.98, 0.95).into();
    c[ImGuiCol::CloseButton as usize] = (0.59, 0.59, 0.59, 0.50).into();
    c[ImGuiCol::CloseButtonHovered as usize] = (0.98, 0.39, 0.36, 1.00).into();
    c[ImGuiCol::CloseButtonActive as usize] = (0.98, 0.39, 0.36, 1.00).into();
    c[ImGuiCol::PlotLines as usize] = (0.39, 0.39, 0.39, 1.00).into();
    c[ImGuiCol::PlotLinesHovered as usize] = (1.00, 0.43, 0.35, 1.00).into();
    c[ImGuiCol::PlotHistogram as usize] = (0.90, 0.70, 0.00, 1.00).into();
    c[ImGuiCol::PlotHistogramHovered as usize] = (1.00, 0.60, 0.00, 1.00).into();
    c[ImGuiCol::TextSelectedBg as usize] = (0.26, 0.59, 0.98, 0.35).into();
    c[ImGuiCol::ModalWindowDarkening as usize] = (0.20, 0.20, 0.20, 0.35).into();

    if dark {
        for color in 0..c.len() {
            use palette::{Hsv, LinSrgb};

            let col = &mut c[color];
            let mut hsv: Hsv = LinSrgb::new(col.x, col.y, col.z).into();

            if hsv.saturation < 0.1 {
                hsv.value = 1.0 - hsv.value;
            }
            let rgb: LinSrgb = hsv.into();
            col.x = rgb.red;
            col.y = rgb.green;
            col.z = rgb.blue;

            if col.w < 1.00 {
                col.w *= alpha;
            }
        }
    } else {
        for color in 0..c.len() {
            let col = &mut c[color];
            if col.w < 1.00 {
                col.x *= alpha;
                col.y *= alpha;
                col.z *= alpha;
                col.w *= alpha;
            }
        }
    }
}

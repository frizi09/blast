use super::super::inspect::{Inspector, InspectorCtx, InspectorRegister};
use inspector::inspector_impl::inspector_macros::inspect_field;
use player_anim::PlayerAnimControlSet;
use specs::shred::Resources;

pub fn register_all(res: &mut Resources) {
    use features::components::*;
    struct_inspector_register!(res, SpellCaster, { abilities });
    struct_inspector_register!(res, MovementInertia, { vel });
    struct_inspector_register!(res, Drag, { [0] });
    struct_inspector_register!(res, Force, { [0] });
    struct_inspector_register!(res, Velocity, { [0] });
    struct_inspector_register!(res, Owner, { [0] });
    struct_inspector_register!(res, PlayerController, {});
    struct_inspector_register!(res, Orbit, { radius, angle, speed });
    struct_inspector_register!(res, Tilemap, { num_cols });

    struct_inspector_register!(res, PlayerAnimControlSet, {
        (animations => |ctx: &InspectorCtx, item: &mut PlayerAnimControlSet| {
            for (anim, control) in item.animations.iter_mut() {
                inspect_field("animation", || {
                    ctx.inspect(anim);
                });
                inspect_field("control set", || {
                    ctx.inspect(control);
                });
            }
        })
    });
}

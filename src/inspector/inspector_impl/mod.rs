#[macro_use]
mod inspector_macros;

mod inspector_impl_builtins;
mod inspector_impl_math;
mod inspector_impl_prim;
mod inspector_impl_struct;

use specs::shred::Resources;

pub use self::inspector_macros::*;

pub fn register_all(res: &mut Resources) {
    use self::*;
    inspector_impl_builtins::register_all(res);
    inspector_impl_math::register_all(res);
    inspector_impl_prim::register_all(res);
    inspector_impl_struct::register_all(res);
}

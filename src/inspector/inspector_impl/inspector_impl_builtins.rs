use super::super::inspect::{Inspector, InspectorCtx, PrimInspectorRegister};
use amethyst::core::{GlobalTransform, Parent, Transform};
use amethyst::renderer::{Material, TextureHandle, TextureOffset};
use amethyst_imgui::ImguiAccess;
use imgui::ImGuiSelectableFlags;
use specs::world::EntitiesRes;
use specs::{Entity, Resources};

pub fn register_all(res: &mut Resources) {
    // lang helpers
    struct_inspector_register_prim!(res, (f32,f32), { [0], [1] });

    // core
    struct_inspector_register!(res, GlobalTransform, { [0] });
    struct_inspector_register!(res, Transform, { rotation, scale, translation });
    struct_inspector_register!(res, Parent, { entity });

    // renderer
    struct_inspector_register!(res, Material, {
        alpha_cutoff,
        albedo,
        albedo_offset,
        emission,
        emission_offset,
        normal,
        normal_offset,
        metallic,
        metallic_offset,
        roughness,
        roughness_offset,
        ambient_occlusion,
        ambient_occlusion_offset,
        caveat,
        caveat_offset
    });
    struct_inspector_register_prim!(res, TextureOffset, { u, v });
    struct_inspector_register_prim!(res, TextureHandle, {
        (id => |ctx: &InspectorCtx, item: &mut TextureHandle| {
            ctx.inspect(&mut item.id());
        })
    });

    EntityInspector::register_prim(res);
}

#[derive(Default)]
struct EntityInspector;

impl Inspector for EntityInspector {
    type Item = Entity;
    fn get_name(&self) -> &'static str {
        "Entity"
    }
    fn inspect(&self, ctx: &InspectorCtx, entity: &mut Entity) {
        if let Some(ui) = ImguiAccess.ui() {
            let alive = ctx.res.fetch::<EntitiesRes>().is_alive(*entity);

            let entity_name = im_str!("Entity {} \u{f35d}", entity.id());
            ui.push_id(entity_name);

            if alive {
                if ui.selectable(
                    entity_name,
                    false,
                    ImGuiSelectableFlags::empty(),
                    (0.0, 0.0),
                ) {
                    ctx.select_entity(*entity);
                }
            } else {
                ui.text(entity_name);
                ui.same_line(-1.0);
                ui.text_disabled(im_str!("(dead)"));
            }

            ui.pop_id();
        }
    }
}

use amethyst_imgui::{ImguiAccess, UiExt};

macro_rules! prim_inspector {
    ($res:ident, $prim:ty, $f:expr) => {{
        use $crate::inspector::inspect::PrimInspectorRegister;
        <MacrofiedInspector as PrimInspectorRegister>::register_prim($res);

        #[derive(Default)]
        pub struct MacrofiedInspector;
        impl Inspector for MacrofiedInspector {
            type Item = $prim;
            fn get_name(&self) -> &'static str {
                stringify!($prim)
            }
            fn inspect<'b>(&self, _ctx: &InspectorCtx<'b>, item: &mut Self::Item) {
                if let Some(ui) = $crate::amethyst_imgui::ImguiAccess.ui() {
                    $f(item, ui);
                }
            }
        }
    }};
}

macro_rules! prim_inspector_num {
    ($res:ident, $prim:ty, $f:ident) => {
        prim_inspector!($res, $prim, |item: &mut $prim, ui: &::imgui::Ui| {
            let mut local = *item as _;
            let im_id = im_str!("##{}", stringify!($prim));
            ui.$f(im_id, &mut local).build();
            *item = local as $prim;
        });
    };
}

macro_rules! struct_inspector_register {
    ($res:ident, $type:ty, $fields:tt) => {{
        use $crate::inspector::inspect::InspectorRegister;
        struct_inspector!(MacrofiedInspector, $type, $fields);
        <MacrofiedInspector as InspectorRegister>::register($res);
    }};
}

macro_rules! struct_inspector_register_prim {
    ($res:ident, $type:ty, $fields:tt) => {{
        use $crate::inspector::inspect::PrimInspectorRegister;
        struct_inspector!(MacrofiedInspector, $type, $fields);
        <MacrofiedInspector as PrimInspectorRegister>::register_prim($res);
    }};
}

macro_rules! struct_inspector {
    (@fields $args:tt ({ $($field:tt),* })) => {
        || {
            $({
                struct_inspector!(@field $args ($field));
            })*
        }
    };
    (@fields [$ctx:ident, $item:ident] ([$closure:expr])) => {
        || {
            $closure($ctx, $item)
        }
    };
    (@field [$ctx:ident, $item:ident] (($field:ident => $closure:expr))) => {
        $crate::inspector::inspector_impl::inspect_field(stringify!($field), || $closure($ctx, $item));
    };
    (@field [$ctx:ident, $item:ident] ($field:ident)) => {
        $crate::inspector::inspector_impl::inspect_field(stringify!($field), || $ctx.inspect(&mut $item.$field));
    };
    (@field [$ctx:ident, $item:ident] ([$field:tt])) => {
        $crate::inspector::inspector_impl::inspect_field(stringify!($field), || $ctx.inspect(&mut $item.$field));
    };
    ($inspector:ident<$($types:ident),+>, $type:ty, $fields:tt) => {
        #[derive(Default)]
        pub struct $inspector<$($types),+> { marker: ::std::marker::PhantomData<($($types),+)> }
        impl<$($types),+> Inspector for $inspector<$($types),+> where
            $(
                $types: Send + Sync + 'static
             ),+ {
            type Item = $type;
            fn get_name(&self) -> &'static str {
                stringify!($type)
            }
            fn inspect<'b>(&self, _ctx: &InspectorCtx<'b>, _item: &mut Self::Item) {
                $crate::inspector::inspector_impl::inspect_struct(self.get_name(), struct_inspector!(@fields [_ctx, _item] ($fields)));
            }
        }
    };
    ($inspector:ident, $type:ty, $fields:tt) => {
        #[derive(Default)]
        pub struct $inspector;
        impl Inspector for $inspector {
            type Item = $type;
            fn get_name(&self) -> &'static str {
                stringify!($type)
            }
            fn inspect<'b>(&self, _ctx: &InspectorCtx<'b>, _item: &mut Self::Item) {
                $crate::inspector::inspector_impl::inspect_struct(self.get_name(), struct_inspector!(@fields [_ctx, _item] ($fields)));
            }
        }
    }
}

// HELPER FUNCTIONS
pub fn inspect_struct<F: FnOnce()>(name: &str, f: F) {
    if let Some(ui) = ImguiAccess.ui() {
        ui.new_line();
        ui.push_id(im_str!("struct_{}", name));
        ui.indent(0.0);
        ui.begin_group();
        f();
        ui.end_group();
        ui.unindent(0.0);
        ui.pop_id();
    }
}

pub fn inspect_field<F: FnOnce()>(name: &str, f: F) {
    if let Some(ui) = ImguiAccess.ui() {
        ui.text(name);
        ui.push_id(im_str!("field_{}", name));
        ui.same_line(0.0);
        f();
        ui.pop_id();
    }
}

use super::super::inspect::{Inspector, InspectorCtx, PrimInspectorRegister};
use specs::shred::Resources;

pub fn register_all(res: &mut Resources) {
    prim_inspector_num!(res, f32, drag_float);
    prim_inspector_num!(res, f64, drag_float);
    prim_inspector_num!(res, usize, drag_int);
    prim_inspector_num!(res, u8, drag_int);
    prim_inspector_num!(res, u16, drag_int);
    prim_inspector_num!(res, u32, drag_int);
    prim_inspector_num!(res, u64, drag_int);
    prim_inspector_num!(res, u128, drag_int);
    prim_inspector_num!(res, i16, drag_int);
    prim_inspector_num!(res, i32, drag_int);
    prim_inspector_num!(res, i64, drag_int);
    prim_inspector_num!(res, i128, drag_int);
    prim_inspector!(res, bool, |item: &mut bool, ui: &::imgui::Ui| {
        ui.checkbox(im_str!("##bool"), item);
    });
}

use super::super::inspect::{Inspector, InspectorCtx, PrimInspectorRegister};
use amethyst::core::cgmath::{Matrix4, Quaternion, Vector2, Vector3, Vector4};
use amethyst_imgui::ImguiAccess;
use specs::shred::Resources;

pub fn register_all(res: &mut Resources) {
    Vector2InspectorF32::register_prim(res);
    Vector2InspectorF64::register_prim(res);
    Vector3InspectorF32::register_prim(res);
    Vector3InspectorF64::register_prim(res);
    Vector4InspectorF32::register_prim(res);
    Vector4InspectorF64::register_prim(res);
    Matrix4Inspector::<f32>::register_prim(res);
    Matrix4Inspector::<f64>::register_prim(res);
    QuaternionInspectorF32::register_prim(res);
    QuaternionInspectorF64::register_prim(res);
}

struct_inspector!(Vector2InspectorF32, Vector2<f32>, { x, y });
struct_inspector!(Vector2InspectorF64, Vector2<f64>, { x, y });
struct_inspector!(Vector3InspectorF32, Vector3<f32>, { x, y, z });
struct_inspector!(Vector3InspectorF64, Vector3<f64>, { x, y, z });
struct_inspector!(Vector4InspectorF32, Vector4<f32>, { x, y, z, w });
struct_inspector!(Vector4InspectorF64, Vector4<f64>, { x, y, z, w });
struct_inspector!(QuaternionInspectorF32, Quaternion<f32>, { s, v });
struct_inspector!(QuaternionInspectorF64, Quaternion<f64>, { s, v });

struct_inspector!(
    Matrix4Inspector<T>,
    Matrix4<T>,
    [
        |ctx: &InspectorCtx, item: &mut Matrix4<T>| if let Some(ui) = ImguiAccess.ui() {
            macro_rules! inspect_matrix_item {
                ($ctx:ident, $item:tt) => {
                    ui.with_id(im_str!("matrix_{}", stringify!($item)), || {
                        ctx.inspect(&mut $item);
                    });
                };
            }

            let spacing = ui.imgui().style().item_spacing;
            let avail = (ui.get_content_region_avail().0 - spacing.x * 3.0).max(0.0);

            ui.with_item_width(avail / 4.0, || {
                ui.group(|| {
                    inspect_matrix_item!(ctx, (item.x.x));
                    inspect_matrix_item!(ctx, (item.y.x));
                    inspect_matrix_item!(ctx, (item.z.x));
                    inspect_matrix_item!(ctx, (item.w.x));
                });
                ui.same_line(0.0);
                ui.group(|| {
                    inspect_matrix_item!(ctx, (item.x.y));
                    inspect_matrix_item!(ctx, (item.y.y));
                    inspect_matrix_item!(ctx, (item.z.y));
                    inspect_matrix_item!(ctx, (item.w.y));
                });
                ui.same_line(0.0);
                ui.group(|| {
                    inspect_matrix_item!(ctx, (item.x.z));
                    inspect_matrix_item!(ctx, (item.y.z));
                    inspect_matrix_item!(ctx, (item.z.z));
                    inspect_matrix_item!(ctx, (item.w.z));
                });
                ui.same_line(0.0);
                ui.group(|| {
                    inspect_matrix_item!(ctx, (item.x.w));
                    inspect_matrix_item!(ctx, (item.y.w));
                    inspect_matrix_item!(ctx, (item.z.w));
                    inspect_matrix_item!(ctx, (item.w.w));
                });
            })
        }
    ]
);

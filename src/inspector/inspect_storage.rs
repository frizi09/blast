use super::inspect::InspectorCtx;
use specs::shred::{CastFrom, Resource};
use specs::storage::{MaskedStorage, UnprotectedStorage};
use specs::{BitSet, Component, Entity};
use std::any::TypeId;

pub trait InspectStorage {
    fn get_name(&self, ctx: &InspectorCtx) -> &'static str;
    fn get_type_id(&self) -> TypeId;
    fn contains(&self, entity: Entity) -> bool;
    fn inspect(&mut self, entity: Entity, ctx: &InspectorCtx);
    fn mask(&self) -> &BitSet;
}

impl<T> CastFrom<T> for InspectStorage
where
    T: InspectStorage + 'static,
{
    fn cast(t: &T) -> &(InspectStorage + 'static) {
        t
    }

    fn cast_mut(t: &mut T) -> &mut (InspectStorage + 'static) {
        t
    }
}

impl<T> InspectStorage for MaskedStorage<T>
where
    T: Component + Resource,
{
    fn get_name(&self, ctx: &InspectorCtx) -> &'static str {
        ctx.get_name::<T>().unwrap_or("Undefined")
    }
    fn get_type_id(&self) -> TypeId {
        TypeId::of::<Self>()
    }
    fn contains(&self, entity: Entity) -> bool {
        self.mask().contains(entity.id())
    }
    fn inspect(&mut self, entity: Entity, ctx: &InspectorCtx) {
        let e = unsafe {
            let storage: &mut LocalMaskedStorage<T> = ::std::mem::transmute(self);
            storage.inner.get_mut(entity.id())
        };
        ctx.inspect(e);
    }
    fn mask(&self) -> &BitSet {
        let storage: &LocalMaskedStorage<T> = unsafe { ::std::mem::transmute(self) };
        &storage.mask
    }
}

// a local copy of MaskedStorage<T> to access private field
struct LocalMaskedStorage<T: Component> {
    mask: BitSet,
    inner: T::Storage,
}

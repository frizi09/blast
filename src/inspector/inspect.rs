use super::inspect_storage::InspectStorage;
use specs::shred::{FetchMut, MetaTable, Resource, Resources};
use specs::storage::MaskedStorage;
use specs::{Component, Entity, World};
use std::any::TypeId;
use std::cell::RefCell;

pub trait Inspector: Resource {
    type Item: Resource;
    fn get_name(&self) -> &'static str;
    fn inspect(&self, ctx: &InspectorCtx, item: &mut Self::Item);
}

pub trait InspectableStorageWorld {
    fn inspect_storages(res: &Resources) -> FetchMut<MetaTable<InspectStorage>>;
    fn register_inspect_storage<I: Resource + Component>(res: &mut Resources);
}

impl InspectableStorageWorld for World {
    fn register_inspect_storage<I: Resource + Component>(res: &mut Resources) {
        res.fetch_mut::<MetaTable<InspectStorage>>()
            .register(&*res.fetch::<MaskedStorage<I>>());
    }
    fn inspect_storages(res: &Resources) -> FetchMut<MetaTable<InspectStorage>> {
        res.fetch_mut::<MetaTable<InspectStorage>>()
    }
}

struct InspectStack<T> {
    stack: Vec<(TypeId, Box<Inspector<Item = T>>)>,
}

impl<T: Resource> InspectStack<T> {
    pub fn new<I: Inspector<Item = T>>(inspector: I) -> Self {
        Self {
            stack: vec![(TypeId::of::<I>(), Box::new(inspector))],
        }
    }
    pub fn push<I: Inspector<Item = T>>(&mut self, inspector: I) {
        self.stack.push((TypeId::of::<I>(), Box::new(inspector)))
    }

    pub fn get_name(&self) -> &'static str {
        self.stack
            .last()
            .map(|(_, inspector)| inspector.get_name())
            .unwrap()
    }

    pub fn inspect_first(&self, ctx: &InspectorCtx, item: &mut T) {
        self.stack
            .last()
            .map(|(_, inspector)| inspector.inspect(ctx, item))
            .unwrap()
    }

    pub fn inspect_next(&self, prev_id: TypeId, ctx: &InspectorCtx, item: &mut T) {
        self.stack
            .iter()
            .position(|&(id, _)| id == prev_id)
            .and_then(|pos| {
                if pos > 0 {
                    self.stack.get(pos - 1)
                } else {
                    None
                }
            })
            .map(|(_, inspector)| inspector.inspect(ctx, item));
    }
}

pub trait PrimInspectorRegister {
    fn register_prim(res: &mut Resources);
}

pub trait InspectorRegister {
    fn register(res: &mut Resources);
}

impl<I> PrimInspectorRegister for I
where
    I: Inspector + Default + 'static,
{
    fn register_prim(res: &mut Resources) {
        if res.has_value::<InspectStack<I::Item>>() {
            let stack = res.get_mut::<InspectStack<I::Item>>().unwrap();
            stack.push(I::default());
        } else {
            res.insert(InspectStack::new(I::default()));
        }
    }
}

impl<I> InspectorRegister for I
where
    I: Inspector + Default + 'static,
    I::Item: Component,
{
    fn register(res: &mut Resources) {
        <I as PrimInspectorRegister>::register_prim(res);
        World::register_inspect_storage::<I::Item>(res);
    }
}

pub struct InspectorCtx<'a> {
    pub res: &'a Resources,
    pub selected_entity: RefCell<Option<Entity>>,
}

impl<'a> InspectorCtx<'a> {
    pub fn new(res: &'a Resources) -> Self {
        Self {
            res,
            selected_entity: RefCell::new(None),
        }
    }

    pub fn select_entity(&self, entity: Entity) {
        *self.selected_entity.borrow_mut() = Some(entity);
    }

    pub fn get_selected_entity(&self) -> Option<Entity> {
        *self.selected_entity.borrow()
    }

    pub fn get_name<T: Resource>(&self) -> Option<&'static str> {
        self.res
            .try_fetch::<InspectStack<T>>()
            .map(|stack| stack.get_name())
    }

    pub fn inspect<T>(&self, item: &mut T)
    where
        T: Resource,
    {
        if let Some(stack) = self.res.try_fetch::<InspectStack<T>>() {
            stack.inspect_first(self, item);
        }
    }

    #[allow(dead_code)]
    pub fn inspect_next<I>(&self, item: &mut I::Item)
    where
        I: Inspector + 'static,
    {
        let type_id = TypeId::of::<I>();
        if let Some(stack) = self.res.try_fetch::<InspectStack<I::Item>>() {
            stack.inspect_next(type_id, self, item)
        }
    }
}

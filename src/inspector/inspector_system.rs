use super::inspect::{InspectableStorageWorld, InspectorCtx};
use super::inspect_storage::InspectStorage;
use amethyst_imgui::{ImguiAccess, UiExt};
use hibitset::BitSetLike;
use imgui::ImGuiSelectableFlags;
use specs::shred::MetaTable;
use specs::world::Index;
use specs::{BitSet, Entities, Join, Resources, RunNow, SystemData, World};
use std::any::TypeId;
use std::collections::HashSet;

#[derive(Default)]
pub struct InspectWorldSystem {
    storage_filter: HashSet<TypeId>,
    selected_entity_id: Index,
    ui_initialized: bool,
}

impl InspectWorldSystem {
    pub fn new() -> Self {
        Default::default()
    }
}

// INSPECTOR GUI

impl InspectWorldSystem {
    fn storage_filtered(&self, type_id: TypeId) -> bool {
        self.storage_filter.contains(&type_id)
    }
    fn set_storage_filtered(&mut self, type_id: TypeId, filtered: bool) {
        if filtered {
            self.storage_filter.insert(type_id);
        } else {
            self.storage_filter.remove(&type_id);
        }
    }
}

type InspectorData<'a> = (Entities<'a>,);

impl<'a> RunNow<'a> for InspectWorldSystem {
    fn setup(&mut self, res: &mut Resources) {
        use super::inspector_impl;
        res.insert::<MetaTable<InspectStorage>>(MetaTable::new());
        inspector_impl::register_all(res);
        self.ui_initialized = false;
    }

    fn run_now(&mut self, res: &'a Resources) {
        let (entities,) = InspectorData::fetch(res);
        let storages = World::inspect_storages(res);

        if let Some(ui) = ImguiAccess.ui() {
            let ctx = InspectorCtx::new(res);

            ui.window(im_str!("\u{f0c3} Entity Inspector###entity_inspector"))
                .build(|| {
                    let entity_indices = {
                        let used_masks = storages
                            .iter(res)
                            .filter(|storage| self.storage_filtered(storage.get_type_id()))
                            .map(|storage| storage.mask())
                            .collect::<Vec<_>>();

                        if used_masks.len() > 0 {
                            BitSetVecAnd::new(used_masks).iter().collect::<Vec<_>>()
                        } else {
                            BitSetVecOr::new(storages.iter(res).map(|s| s.mask()).collect())
                                .iter()
                                .collect::<Vec<_>>()
                        }
                    };

                    ui.columns(2, im_str!("entities_table"), true);
                    if !self.ui_initialized {
                        ui.set_column_offset(1, 150.0);
                    }

                    ui.text(im_str!("Entities"));
                    ui.next_column();
                    ui.text(im_str!("Entity {}", self.selected_entity_id));
                    ui.separator();
                    ui.next_column();

                    ui.scroll_frame(im_str!("Filter components"), (-1.0, 200.0), || {
                        for storage in storages.iter(res) {
                            let initial_filter = self.storage_filtered(storage.get_type_id());
                            let name = storage.get_name(&ctx);

                            let change_filter = ui.selectable(
                                im_str!("{}", name),
                                initial_filter,
                                ImGuiSelectableFlags::empty(),
                                (0.0, 0.0),
                            );
                            if change_filter {
                                self.set_storage_filtered(storage.get_type_id(), !initial_filter);
                            }
                        }
                    });
                    ui.spacing();
                    ui.text("Select entity");
                    ui.scroll_frame(im_str!("Entity selector"), (-1.0, -1.0), || {
                        for entity in entity_indices.into_iter() {
                            if ui.selectable(
                                im_str!("Entity {}", entity),
                                self.selected_entity_id == entity,
                                ImGuiSelectableFlags::empty(),
                                (0.0, 0.0),
                            ) {
                                self.selected_entity_id = entity
                            }
                        }
                    });

                    ui.next_column();
                    ui.push_item_width(-1.0);
                    ui.child_frame(im_str!("Entity details"), (-1.0, -1.0))
                        .build(|| {
                            let selected_entity = (&*entities,)
                                .join()
                                .find(|e| e.0.id() == self.selected_entity_id);
                            if let Some((entity,)) = selected_entity {
                                ctx.select_entity(entity);

                                for storage in storages.iter_mut(res) {
                                    if storage.contains(entity) {
                                        let opened = ui.collapsing_header(im_str!(
                                            "{}",
                                            storage.get_name(&ctx)
                                        )).build();
                                        if opened {
                                            ui.same_line(0.0);
                                            storage.inspect(entity, &ctx);
                                        }
                                    }
                                }

                                if let Some(new_selection) = ctx.get_selected_entity() {
                                    if entity != new_selection {
                                        self.selected_entity_id = new_selection.id();
                                    }
                                }
                            } else {
                                ui.text(im_str!("Entity not found"));
                            }
                        });
                    ui.pop_item_width();
                    ui.columns(1, im_str!("entities_table"), true);
                });

            self.ui_initialized = true;
        }
    }
}

// BITSET
struct BitSetVecAnd<'a> {
    vec: Vec<&'a BitSet>,
}

impl<'a> BitSetVecAnd<'a> {
    fn new(vec: Vec<&'a BitSet>) -> Self {
        Self { vec }
    }
}

impl<'a> BitSetLike for BitSetVecAnd<'a> {
    #[inline]
    fn layer3(&self) -> usize {
        self.vec
            .iter()
            .fold(usize::max_value(), |l, c| l & c.layer3())
    }
    #[inline]
    fn layer2(&self, i: usize) -> usize {
        self.vec
            .iter()
            .fold(usize::max_value(), |l, c| l & c.layer2(i))
    }
    #[inline]
    fn layer1(&self, i: usize) -> usize {
        self.vec
            .iter()
            .fold(usize::max_value(), |l, c| l & c.layer1(i))
    }
    #[inline]
    fn layer0(&self, i: usize) -> usize {
        self.vec
            .iter()
            .fold(usize::max_value(), |l, c| l & c.layer0(i))
    }
    #[inline]
    fn contains(&self, i: Index) -> bool {
        self.vec.iter().fold(true, |l, c| l && c.contains(i))
    }
}

struct BitSetVecOr<'a> {
    vec: Vec<&'a BitSet>,
}

impl<'a> BitSetVecOr<'a> {
    fn new(vec: Vec<&'a BitSet>) -> Self {
        Self { vec }
    }
}

impl<'a> BitSetLike for BitSetVecOr<'a> {
    #[inline]
    fn layer3(&self) -> usize {
        self.vec.iter().fold(0, |l, c| l | c.layer3())
    }
    #[inline]
    fn layer2(&self, i: usize) -> usize {
        self.vec.iter().fold(0, |l, c| l | c.layer2(i))
    }
    #[inline]
    fn layer1(&self, i: usize) -> usize {
        self.vec.iter().fold(0, |l, c| l | c.layer1(i))
    }
    #[inline]
    fn layer0(&self, i: usize) -> usize {
        self.vec.iter().fold(0, |l, c| l | c.layer0(i))
    }
    #[inline]
    fn contains(&self, i: Index) -> bool {
        self.vec.iter().fold(false, |l, c| l || c.contains(i))
    }
}

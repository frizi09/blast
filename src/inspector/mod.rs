use amethyst::core::{Result, SystemBundle};
use specs::DispatcherBuilder;

mod inspect;
mod inspect_storage;
mod inspector_impl;
mod inspector_system;

pub use self::inspector_system::InspectWorldSystem;

#[derive(Default)]
pub struct InspectorBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for InspectorBundle {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add_thread_local(InspectWorldSystem::new());
        Ok(())
    }
}

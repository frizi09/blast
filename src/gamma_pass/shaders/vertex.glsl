#version 150 core

in vec3 position;
in vec2 tex_coord;

out VertexData {
  vec2 tex_coord;
} vertex;

void main() {
    vertex.tex_coord = tex_coord;
    gl_Position = vec4(position, 0.0);
}

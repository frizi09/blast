#version 150 core

// uniform sampler2D pre_fx;

in VertexData {
  vec2 tex_coord;
} vertex;

out vec4 color;

void main() {
  color = vec4(0.5, 0.9, 0.3, 0.5);
    // color = texture(pre_fx, vertex.tex_coord) * 0.5 + vec4(0.5, 0.5, 0.5, 0.5);
}

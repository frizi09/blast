use amethyst::renderer::error::Result;
use amethyst::renderer::pipe::pass::{Pass, PassData};
use amethyst::renderer::pipe::{Effect, NewEffect};
use amethyst::renderer::{
    ColorMask, DepthMode, Encoder, Factory, Mesh, PosTex, VertexFormat, ALPHA,
};

const VERT_SRC: &[u8] = include_bytes!("shaders/vertex.glsl");
const FRAG_SRC: &[u8] = include_bytes!("shaders/frag.glsl");

pub struct GammaPass {
    mesh: Option<Mesh>,
}

impl GammaPass {
    pub fn new() -> Self {
        GammaPass { mesh: None }
    }
}

impl<'a> PassData<'a> for GammaPass {
    type Data = ();
}

impl Pass for GammaPass {
    fn compile(&mut self, mut effect: NewEffect) -> Result<Effect> {
        self.mesh = Some(Mesh::build(SCREEN_QUAD).build(&mut effect.factory)?);

        effect
            .simple(VERT_SRC, FRAG_SRC)
            .with_raw_vertex_buffer(PosTex::ATTRIBUTES, PosTex::size() as u8, 0)
            // .with_texture("pre_fx")
            .with_output("color", Some(DepthMode::LessEqualWrite))
            .build()
    }
    fn apply<'a, 'b: 'a>(
        &'a mut self,
        encoder: &mut Encoder,
        effect: &mut Effect,
        _factory: Factory,
        data: <Self as PassData<'a>>::Data,
    ) {
        let mesh = self.mesh.as_ref().unwrap();
        let vbuf = mesh.buffer(PosTex::ATTRIBUTES).unwrap().clone();

        effect.data.vertex_bufs.push(vbuf);
        effect.draw(mesh.slice(), encoder);
    }
}

const SCREEN_QUAD: &[PosTex] = &[
    PosTex {
        position: [0., 1., 0.],
        tex_coord: [0., 0.],
    },
    PosTex {
        position: [1., 1., 0.],
        tex_coord: [1., 0.],
    },
    PosTex {
        position: [1., 0., 0.],
        tex_coord: [1., 1.],
    },
    PosTex {
        position: [0., 1., 0.],
        tex_coord: [0., 0.],
    },
    PosTex {
        position: [1., 0., 0.],
        tex_coord: [1., 1.],
    },
    PosTex {
        position: [0., 0., 0.],
        tex_coord: [0., 1.],
    },
];
